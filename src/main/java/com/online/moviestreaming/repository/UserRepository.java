package com.online.moviestreaming.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.online.moviestreaming.entity.User;

/**
 * 
 * @author janbee
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

	/**
	 * 
	 * @param email
	 * @param password
	 * @return User object
	 */
	public Optional<User> findByEmailAndPassword(String email, String password);
}
