package com.online.moviestreaming.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.online.moviestreaming.entity.User;

@Repository
public interface RegistrationRepository extends JpaRepository<User, Integer> {

}
