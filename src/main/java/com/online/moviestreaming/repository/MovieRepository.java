package com.online.moviestreaming.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.online.moviestreaming.entity.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer>{

	@Query(value="select * from movie u where  u.movie_title like %:keyword% or u.movie_genre like %:keyword%", nativeQuery=true)
	public List<Movie> searchMovies(@Param("keyword") String keyword);

}
