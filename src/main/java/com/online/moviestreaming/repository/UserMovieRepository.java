package com.online.moviestreaming.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.online.moviestreaming.entity.UserMovie;

public interface UserMovieRepository extends JpaRepository<UserMovie, Integer>{

	public UserMovie findByUserIdAndMovieId(Integer userId, Integer movieId);

	@Query(value = "select * from USER_MOVIE um where um.user_id=:userId", nativeQuery = true)
	public List<UserMovie> userWachedMovieList(@Param("userId") Integer userId);

}
