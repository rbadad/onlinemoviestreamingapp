package com.online.moviestreaming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineMovieStreamingApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineMovieStreamingApplication.class, args);
	}

}
