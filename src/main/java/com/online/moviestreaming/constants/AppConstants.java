package com.online.moviestreaming.constants;

public class AppConstants {


	public static final String LOGIN_SUCCESS ="Login successful";
	public static final String LOGIN_SUCCESS_STATUS_CODE ="2000";
	public static final String LOGIN_FAILURE ="invalid email and password";
	public static final String LOGIN_FAILURE_STATUS_CODE ="2001";
	public static final String REGISTRATION_EXPIRED ="Registration period ended, please register again!";
	public static final String EXPIRED_STATUS_CODE ="2002";
	public static final String TRIAL_USER = "trial";

	public static final String MOVIES_NOT_AVAILABLE_MSG = "NO MOVIES ARE STREAMING NOW";
	public static final String MOVIES_NOT_AVAILABLE_CODE = "1700";

	public static final String MOVIE_NOT_FOUND = "MOVIE IS NOT AVAILABLE FOR STREAMING";
	public static final String MOVIE_NOT_FOUND_STATUS_CODE = "1500";

	public static final String USER_NOT_FOUND = "USER NOT FOUND";
	public static final String USER_NOT_FOUND_STATUS_CODE = "1600";

	public static final String USER_MOVIE_STATUS_CODE = "1601";
	public static final String USER_MOVIE_STATUS_MSG = "USER HAS WATCHED MOVIE";

	public static final String USER_MOVIE_STATUS_CODE_FAILURE = "1602";
	public static final String USER_MOVIE_STATUS_MSG_FAILURE = "NO USER WATCHED MOVIES FOUND";

	public static final String SEARCH_NOT_FOUND_STATUS_MSG = "NO MOVIES FOUND WITH SEARCH KEY";
	public static final String SEARCH_NOT_FOUND_STATUS_CODE = "1800";

	public static final String SEARCH_NOT_FOUND = "NO MOVIES FOUND WITH SEARCH KEY";

	public static final String REGISTRATION_SUCCESS = "User registered successfully";
	public static final String REGISTRATION_FAILURE = "User registration failed ";
	public static final String STATUS_CODE_SUCCESS = "1000";
	public static final String STATUS_CODE_FAILURE = "400";
	public static final String USER_STATUS = "active";

}
