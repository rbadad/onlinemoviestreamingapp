package com.online.moviestreaming.service;

import com.online.moviestreaming.dto.LoginRequestDto;
import com.online.moviestreaming.dto.ResponseDto;

/**
 * 
 * @author janbee
 *
 */
@FunctionalInterface
public interface UserService {

	/**
	 * 
	 * @param requestDto
	 * @return ResponseDto
	 */
	public ResponseDto login(LoginRequestDto requestDto);
}
