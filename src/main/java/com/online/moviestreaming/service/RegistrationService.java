package com.online.moviestreaming.service;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.online.moviestreaming.constants.AppConstants;
import com.online.moviestreaming.dto.UserRegistrationRequestDto;
import com.online.moviestreaming.dto.UserRegistrationResponseDto;
import com.online.moviestreaming.entity.User;
import com.online.moviestreaming.repository.RegistrationRepository;
/**
 * @author prabirkumar.jena
 *
 */
@Service
public class RegistrationService {

	private static final Logger log = LoggerFactory.getLogger(RegistrationService.class);
	/**
	 * registrationRepository for Register the User
	 */
	@Autowired
	private RegistrationRepository registrationRepository;

	public UserRegistrationResponseDto registerUser(UserRegistrationRequestDto requestDto) {
		log.info("inside registerUser service method");
		User userEntity = null;
		UserRegistrationResponseDto responseDto = new UserRegistrationResponseDto();
		User user = new User();
		user.setUserName(requestDto.getUserName());
		user.setPassword(requestDto.getPassword());
		user.setMobileNo(requestDto.getMobileNo());
		user.setEmail(requestDto.getEmail());
		user.setUserType(requestDto.getUserType());
		user.setRegistrationDate(LocalDate.now());
		user.setUserStatus(AppConstants.USER_STATUS);
		try {
			userEntity = registrationRepository.save(user);
		} catch (Exception e) {
			responseDto.setStatusCode(AppConstants.STATUS_CODE_FAILURE);
			responseDto.setMessage(AppConstants.REGISTRATION_FAILURE);
		}

		if (userEntity != null) {
			log.debug("User with his id " + userEntity.getUserId());
			responseDto.setStatusCode(AppConstants.STATUS_CODE_SUCCESS);
			responseDto.setMessage(AppConstants.REGISTRATION_SUCCESS);
		}
		log.info("End of register service method");
		return responseDto;

	}
}
