package com.online.moviestreaming.service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.online.moviestreaming.constants.AppConstants;
import com.online.moviestreaming.dto.LoginRequestDto;
import com.online.moviestreaming.dto.ResponseDto;
import com.online.moviestreaming.entity.User;
import com.online.moviestreaming.repository.UserRepository;

/**
 * 
 * @author janbee
 *
 */
@Service
public class UserServiceImpl implements UserService{

	@Autowired
	UserRepository userRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	/**
	 * 
	 * @param requestDto
	 * @return ResponseDto
	 */
	@Override
	public ResponseDto login(LoginRequestDto requestDto) {
		LOGGER.info("UserServiceImpl :: login :: start");
		ResponseDto responseDto = new ResponseDto();
		Optional<User> user = userRepository.findByEmailAndPassword(requestDto.getEmail(), requestDto.getPassword());
		if(!user.isPresent()) {
			responseDto.setStatusCode(AppConstants.LOGIN_FAILURE_STATUS_CODE);
			responseDto.setStatusMsg(AppConstants.LOGIN_FAILURE);
			return responseDto;
		}
		User userObj = user.get();
		String userType = userObj.getUserType();
		LocalDate registeredDate = userObj.getRegistrationDate();
		long daysbetween = ChronoUnit.DAYS.between(registeredDate, LocalDate.now());
		if(userType.equals(AppConstants.TRIAL_USER) && daysbetween>=1) {
			userObj.setUserStatus("inactive");
			userRepository.save(userObj);
			responseDto.setStatusCode(AppConstants.EXPIRED_STATUS_CODE);
			responseDto.setStatusMsg(AppConstants.REGISTRATION_EXPIRED);
			return responseDto;
		} 
		responseDto.setStatusCode(AppConstants.LOGIN_SUCCESS_STATUS_CODE);
		responseDto.setStatusMsg(AppConstants.LOGIN_SUCCESS);
		responseDto.setUserId(userObj.getUserId());
		LOGGER.info("UserServiceImpl :: login :: end");
		return responseDto;
	}
}
