package com.online.moviestreaming.service;

import com.online.moviestreaming.dto.UserMovieReqDto;
import com.online.moviestreaming.exception.MovieNotFoundException;

public interface UserMovieService {

	public Object getUserMovieDetails(Integer userId) throws MovieNotFoundException;
	public Object getAllUserWatchedMovieList(UserMovieReqDto userMovieReqDto) throws MovieNotFoundException;
}
