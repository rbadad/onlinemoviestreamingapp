package com.online.moviestreaming.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.online.moviestreaming.constants.AppConstants;
import com.online.moviestreaming.dto.UserMovieDetailsResponse;
import com.online.moviestreaming.dto.UserMovieReqDto;
import com.online.moviestreaming.dto.UserWatchedMovieDto;
import com.online.moviestreaming.entity.Movie;
import com.online.moviestreaming.entity.User;
import com.online.moviestreaming.entity.UserMovie;
import com.online.moviestreaming.exception.MovieNotFoundException;
import com.online.moviestreaming.exception.UserNotFoundException;
import com.online.moviestreaming.repository.MovieRepository;
import com.online.moviestreaming.repository.RegistrationRepository;
import com.online.moviestreaming.repository.UserMovieRepository;

@Service
public class UserMovieServiceImpl implements UserMovieService{

	@Autowired
	private UserMovieRepository userMovieRepository;

	@Autowired
	private RegistrationRepository registrationRepository;

	@Autowired
	private MovieRepository movieRepository;

	@Override
	public Object getUserMovieDetails(Integer userId) throws MovieNotFoundException {
		UserMovieDetailsResponse umrd = new UserMovieDetailsResponse();
		List<UserMovie> movieList =  userMovieRepository.userWachedMovieList(userId);
		List<UserWatchedMovieDto> movieWatchedList = new ArrayList<UserWatchedMovieDto>();
		if(null!=movieList && movieList.size()>0) {
			for (UserMovie userMovie : movieList) {
				UserWatchedMovieDto userWatchedMovieDto = new UserWatchedMovieDto();
				if(!movieList.isEmpty() && movieList.size()>0) {
					Optional<User> user = registrationRepository.findById(userMovie.getUserId());
					userWatchedMovieDto.setUserName(user.get().getUserName());
				}
				Optional<Movie> movie1 = movieRepository.findById(userMovie.getMovieId());
				if(movie1.isPresent()) {
					userWatchedMovieDto.setMovieName(movie1.get().getMovieName());
				}
				userWatchedMovieDto.setMovieWatchedDate(userMovie.getMovieWatchedDate());
				movieWatchedList.add(userWatchedMovieDto);
			}
		}
		else {
			umrd.setStatusCode(AppConstants.MOVIE_NOT_FOUND_STATUS_CODE);
			umrd.setStatusMsg(AppConstants.MOVIE_NOT_FOUND);
			return umrd;
		}

		return movieWatchedList;
	}

	public Object getAllUserWatchedMovieList(UserMovieReqDto userMovieReqDto) throws MovieNotFoundException {
		UserMovie um = new UserMovie();
		UserMovieDetailsResponse userMovieDetailsResponse  = new UserMovieDetailsResponse();
		if(userMovieReqDto.getMovieId()!=null || userMovieReqDto.getUserId()!=null) {
			UserMovie userMovie = userMovieRepository.findByUserIdAndMovieId(userMovieReqDto.getUserId(), userMovieReqDto.getMovieId());
			if(!Objects.isNull(userMovie)) {
				userMovie.setMovieWatchedDate(LocalDateTime.now());
				userMovieRepository.save(userMovie);
				userMovieDetailsResponse.setStatusCode(AppConstants.USER_MOVIE_STATUS_CODE);
				userMovieDetailsResponse.setStatusMsg(AppConstants.USER_MOVIE_STATUS_MSG);
			}else {
				um.setMovieWatchedDate(LocalDateTime.now());
				Optional<User> user1 = registrationRepository.findById(userMovieReqDto.getUserId());
				if(user1.isPresent()) {
					um.setUserId(user1.get().getUserId());
				}else {
					userMovieDetailsResponse.setStatusCode(AppConstants.USER_NOT_FOUND_STATUS_CODE);
					userMovieDetailsResponse.setStatusMsg(AppConstants.USER_NOT_FOUND);
					return userMovieDetailsResponse;
				}
				Optional<Movie> movie1 = movieRepository.findById(userMovieReqDto.getMovieId());
				if(movie1.isPresent()) {
					um.setMovieId(movie1.get().getMovieId());
				}else {
					userMovieDetailsResponse.setStatusCode(AppConstants.MOVIE_NOT_FOUND_STATUS_CODE);
					userMovieDetailsResponse.setStatusMsg(AppConstants.MOVIE_NOT_FOUND);
					return userMovieDetailsResponse;
				}
				userMovieRepository.save(um);
				userMovieDetailsResponse.setStatusCode(AppConstants.USER_MOVIE_STATUS_CODE);
				userMovieDetailsResponse.setStatusMsg(AppConstants.USER_MOVIE_STATUS_MSG);
			}
		}else {
			userMovieDetailsResponse.setStatusCode(AppConstants.USER_MOVIE_STATUS_CODE_FAILURE);
			userMovieDetailsResponse.setStatusMsg(AppConstants.USER_MOVIE_STATUS_MSG_FAILURE);
		}
		return userMovieDetailsResponse;
		
	}
}
