package com.online.moviestreaming.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.online.moviestreaming.constants.AppConstants;
import com.online.moviestreaming.dto.MovieResponseDto;
import com.online.moviestreaming.entity.Movie;
import com.online.moviestreaming.exception.MovieNotFoundException;
import com.online.moviestreaming.repository.MovieRepository;

@Service
public class MovieServiceImpl  implements MovieService{

	@Autowired
	private MovieRepository movieRepository; 

	@Override
	public Object getAllMovies() {
		MovieResponseDto movieResponseDto = new MovieResponseDto();
		List<Movie> movies =  movieRepository.findAll();
		if(movies.size()>0 && !movies.isEmpty()) {
			return movies;
		}else {
			movieResponseDto.setStatusCode(AppConstants.MOVIES_NOT_AVAILABLE_CODE);
			movieResponseDto.setStatusMsg(AppConstants.MOVIES_NOT_AVAILABLE_MSG);
			return movieResponseDto;
		}
	}

	public Object getMoviedBasedOnId(Integer movieId) throws MovieNotFoundException {
		MovieResponseDto movieResponseDto = new MovieResponseDto();
		Optional<Movie> movie = null;
		try {
			movie =  movieRepository.findById(movieId);	
			if(movie.isPresent()) {
				movie.get();
			}else {
				movieResponseDto.setStatusCode(AppConstants.MOVIE_NOT_FOUND_STATUS_CODE);
				movieResponseDto.setStatusMsg(AppConstants.MOVIE_NOT_FOUND);
				return movieResponseDto;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return movie.get();
	}

	public Object searchMovies(String keyword) throws MovieNotFoundException{
		MovieResponseDto movieResponseDto = new MovieResponseDto();
		List<Movie> movieList = movieRepository.searchMovies(keyword);
		if(null!=movieList && !movieList.isEmpty()) {
			return movieList;
		}else {
			movieResponseDto.setStatusCode(AppConstants.SEARCH_NOT_FOUND_STATUS_CODE);
			movieResponseDto.setStatusMsg(AppConstants.SEARCH_NOT_FOUND_STATUS_MSG);
			return movieResponseDto;
		}

	}

}
