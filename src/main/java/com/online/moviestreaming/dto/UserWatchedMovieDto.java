package com.online.moviestreaming.dto;

import java.time.LocalDateTime;

public class UserWatchedMovieDto {
	
	private String userName;
	private String movieName;
	private LocalDateTime movieWatchedDate;
	
	public UserWatchedMovieDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public LocalDateTime getMovieWatchedDate() {
		return movieWatchedDate;
	}
	public void setMovieWatchedDate(LocalDateTime movieWatchedDate) {
		this.movieWatchedDate = movieWatchedDate;
	}
}
