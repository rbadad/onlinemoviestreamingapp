package com.online.moviestreaming.dto;

public class MovieResponseDto {
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	private String statusCode;
	private String statusMsg;
	public MovieResponseDto() {
		super();
		// TODO Auto-generated constructor stub
	}

}
