package com.online.moviestreaming.dto;

public class MovieSearchDto {
	
	private String movieTitle;
	private String movieGenre;
	public MovieSearchDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getMovieTitle() {
		return movieTitle;
	}
	public void setMovieTitle(String movieTitle) {
		this.movieTitle = movieTitle;
	}
	public String getMovieGenre() {
		return movieGenre;
	}
	public void setMovieGenre(String movieGenre) {
		this.movieGenre = movieGenre;
	}

}
