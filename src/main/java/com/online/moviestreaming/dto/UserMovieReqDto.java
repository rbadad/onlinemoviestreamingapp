package com.online.moviestreaming.dto;

public class UserMovieReqDto {
	
	private Integer userId;
	private Integer movieId;
	public UserMovieReqDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getMovieId() {
		return movieId;
	}
	public void setMovieId(Integer movieId) {
		this.movieId = movieId;
	}
}
