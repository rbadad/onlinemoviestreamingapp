package com.online.moviestreaming.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.online.moviestreaming.dto.LoginRequestDto;
import com.online.moviestreaming.dto.ResponseDto;
import com.online.moviestreaming.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author janbee
 *
 */
@Api("Operations pertaining to users")
@RestController
@RequestMapping("/users")
public class UserController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	UserService userService;
	
	/**
	 * 
	 * @param requestDto
	 * @return ResponseDto
	 */
	@ApiOperation("user login")
	@PostMapping("/login")
	public ResponseEntity<ResponseDto> login(@RequestBody LoginRequestDto requestDto) {
		LOGGER.debug("UserController :: login :: start");
		ResponseDto response = userService.login(requestDto);
		LOGGER.debug("UserController :: login :: end");
		return new ResponseEntity<>(response,HttpStatus.OK);
	}

}
