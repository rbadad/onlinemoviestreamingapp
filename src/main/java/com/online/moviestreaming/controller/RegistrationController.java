package com.online.moviestreaming.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.online.moviestreaming.dto.UserRegistrationRequestDto;
import com.online.moviestreaming.dto.UserRegistrationResponseDto;
import com.online.moviestreaming.exception.UserRegistrationFaliedException;
import com.online.moviestreaming.service.RegistrationService;

import io.swagger.annotations.ApiOperation;

/**
 * This class act as controller for User registration functionality.
 * 
 * @author prabirkumar.jena
 *
 */
@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class RegistrationController {

	/**
	 * log.
	 */
	private static final Logger log = LoggerFactory.getLogger(RegistrationController.class);

	/**
	 * registrationService called Here
	 */
	@Autowired
	private RegistrationService registrationService;

	/**
	 * This controller is for User registration.
	 * 
	 * @return UserRegistrationResponseDto,Should not be null.
	 * @throws UserRegistrationFaliedException
	 */
	@PostMapping("/registration")
	@ApiOperation("User Registration api")
	public ResponseEntity<UserRegistrationResponseDto> registerUser(
			@Valid @RequestBody UserRegistrationRequestDto requestDto) throws UserRegistrationFaliedException {
		log.info("inside registerUser() method ");
		log.debug("User Name is : " + requestDto.getUserName());
		UserRegistrationResponseDto userRegistrationResponseDto = registrationService.registerUser(requestDto);
		log.info("End of registerUser() method");

		return new ResponseEntity<UserRegistrationResponseDto>(userRegistrationResponseDto, HttpStatus.OK);
	}
}
