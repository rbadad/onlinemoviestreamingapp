package com.online.moviestreaming.controller;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.online.moviestreaming.exception.MovieNotFoundException;
import com.online.moviestreaming.service.MovieServiceImpl;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/movies")
public class MovieController {

	private static Logger LOGGER = LoggerFactory.getLogger(MovieController.class);

	@Autowired
	private MovieServiceImpl movieServiceImpl;

	@GetMapping("")
	@ApiOperation("TO show all the available movies to users")
	public ResponseEntity<Object> getAllMovies() {
		LOGGER.info("Inside GetAllAvaailable movies:::::::start");
		Object movieList = new ArrayList<>();
		movieList =  movieServiceImpl.getAllMovies();
		return new ResponseEntity<Object>(movieList, HttpStatus.OK);
	}

	@GetMapping("/{movieId}")
	@ApiOperation("Getting movies details bsed on movies id")
	public ResponseEntity<Object> getMovieById(@PathVariable("movieId") Integer movieId) {

		LOGGER.info("Inside getMovieById movies:::::::start");
		Object movie= null;
		try {
			movie = movieServiceImpl.getMoviedBasedOnId(movieId);
		} catch (Exception e) {
		}
		return new ResponseEntity<Object>(movie, HttpStatus.OK);

	}

	@GetMapping("/searching")
	@ApiOperation("we can search movies information based on movie tile and movie genre")
	public ResponseEntity<Object> searchMovies(@RequestParam(name = "keyword") String keyword) {
		LOGGER.info("Inside searchMovies movies:::::::start");
		Object movieList= new ArrayList<>();
		if(null!=keyword && !keyword.isEmpty()) {		
			try {
				movieList = movieServiceImpl.searchMovies(keyword);
			} catch (MovieNotFoundException e) {
				e.printStackTrace();
			}
		}
		LOGGER.info("Inside searchMovies movies:::::::End");
		return new ResponseEntity<Object>(movieList, HttpStatus.OK);
	}
}
