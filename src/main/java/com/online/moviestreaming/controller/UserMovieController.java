package com.online.moviestreaming.controller;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.online.moviestreaming.dto.UserMovieDetailsResponse;
import com.online.moviestreaming.dto.UserMovieReqDto;
import com.online.moviestreaming.dto.UserWatchedMovieDto;
import com.online.moviestreaming.exception.MovieNotFoundException;
import com.online.moviestreaming.service.UserMovieServiceImpl;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/user-movies")
public class UserMovieController {


	private static Logger LOGGER = LoggerFactory.getLogger(UserMovieController.class);

	@Autowired
	private UserMovieServiceImpl userMovieServiceImpl;
	
	@GetMapping("/{userId}")
	@ApiOperation("TO see how many movies watched by particular user")
	public ResponseEntity<Object>  getUserWatchedMovieDetails(@PathVariable("userId") Integer userId) {
		LOGGER.info("!!!!!!!!!!!!!!Inside getUserWatchedMovieDetails!!!!!!! entered userId is :::START" + userId);
		Object watchedMovieList = new ArrayList<UserWatchedMovieDto>();
		try {
			watchedMovieList = userMovieServiceImpl.getUserMovieDetails(userId);
		} catch (MovieNotFoundException e) {
			e.printStackTrace();
		}
		LOGGER.info("***********************::::END");
		return new ResponseEntity<Object>(watchedMovieList,HttpStatus.OK);
	}
	
	@PostMapping("")
	@ApiOperation("Adding to list of watched movies")
	public ResponseEntity<Object> userWatchedMovieList(@RequestBody UserMovieReqDto userMovieReqDto) {
		LOGGER.info("******************Inside userWatchedMovieList*************::::START" + userMovieReqDto);
		Object userMovieDetailsResponse = new UserMovieDetailsResponse();
		try {
			userMovieDetailsResponse = userMovieServiceImpl.getAllUserWatchedMovieList(userMovieReqDto);
		} catch (MovieNotFoundException e) {
			e.printStackTrace();
		}
		LOGGER.info("******************Inside userWatchedMovieList*********:::END");
		return new ResponseEntity<Object>(userMovieDetailsResponse, HttpStatus.OK);
	}
}
