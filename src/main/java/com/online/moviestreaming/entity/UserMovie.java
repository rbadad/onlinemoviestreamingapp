package com.online.moviestreaming.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "USER_MOVIE")
public class UserMovie implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	Integer userMovieId;
	private Integer userId;
	private Integer movieId;
	private LocalDateTime movieWatchedDate;
	
	public UserMovie() {
		super();
	}

	public UserMovie(Integer userMovieId, Integer userId, Integer movieId, LocalDateTime movieWatchedDate) {
		super();
		this.userMovieId = userMovieId;
		this.userId = userId;
		this.movieId = movieId;
		this.movieWatchedDate = movieWatchedDate;
	}

	public Integer getUserMovieId() {
		return userMovieId;
	}

	public void setUserMovieId(Integer userMovieId) {
		this.userMovieId = userMovieId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getMovieId() {
		return movieId;
	}

	public void setMovieId(Integer movieId) {
		this.movieId = movieId;
	}

	public LocalDateTime getMovieWatchedDate() {
		return movieWatchedDate;
	}

	public void setMovieWatchedDate(LocalDateTime movieWatchedDate) {
		this.movieWatchedDate = movieWatchedDate;
	}

	@Override
	public String toString() {
		return "UserMovie [userMovieId=" + userMovieId + ", userId=" + userId + ", movieId=" + movieId
				+ ", movieWatchedDate=" + movieWatchedDate + "]";
	}	
}
