package com.online.moviestreaming.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MOVIE")
public class Movie implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Integer movieId;
	private String movieName;
	private String movieTitle;
	private String movieDesc;
	private String movieGenre;
	private String movieLanguage;
	private Integer movieReleaseYear;
	
	public Movie() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Movie(Integer movieId, String movieName, String movieTitle, String movieDesc, String movieGenre,
			String movieLanguage, Integer movieReleaseYear) {
		super();
		this.movieId = movieId;
		this.movieName = movieName;
		this.movieTitle = movieTitle;
		this.movieDesc = movieDesc;
		this.movieGenre = movieGenre;
		this.movieLanguage = movieLanguage;
		this.movieReleaseYear = movieReleaseYear;
	}

	public Integer getMovieId() {
		return movieId;
	}

	public void setMovieId(Integer movieId) {
		this.movieId = movieId;
	}

	public String getMovieName() {
		return movieName;
	}

	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}

	public String getMovieTitle() {
		return movieTitle;
	}

	public void setMovieTitle(String movieTitle) {
		this.movieTitle = movieTitle;
	}

	public String getMovieDesc() {
		return movieDesc;
	}

	public void setMovieDesc(String movieDesc) {
		this.movieDesc = movieDesc;
	}

	public String getMovieGenre() {
		return movieGenre;
	}

	public void setMovieGenre(String movieGenre) {
		this.movieGenre = movieGenre;
	}

	public String getMovieLanguage() {
		return movieLanguage;
	}

	public void setMovieLanguage(String movieLanguage) {
		this.movieLanguage = movieLanguage;
	}

	public Integer getMovieReleaseYear() {
		return movieReleaseYear;
	}

	public void setMovieReleaseYear(Integer movieReleaseYear) {
		this.movieReleaseYear = movieReleaseYear;
	}

	@Override
	public String toString() {
		return "Movie [movieId=" + movieId + ", movieName=" + movieName + ", movieTitle=" + movieTitle + ", movieDesc="
				+ movieDesc + ", movieGenre=" + movieGenre + ", movieLanguage=" + movieLanguage + ", movieReleaseYear="
				+ movieReleaseYear + "]";
	}
	
}
