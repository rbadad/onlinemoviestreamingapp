package com.online.moviestreaming.entity;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author prabirkumar.jena
 *
 */
@Entity
@Table(name = "USERS")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "USER_ID")
	@NotNull
	private Integer userId;

	@Column(name = "USER_NAME")
	@NotEmpty(message = "UserName is required")
	private String userName;

	@Column(name = "PASSWORD")
	@NotEmpty(message = "Password is required")
	private String password;

	@Column(name = "MOBILE_NO", unique = true)
	@Size(min = 0, max = 10)
	@NotEmpty(message = "MobileNo is required")
	private String mobileNo;

	@Column(name = "EMAIL", unique = true)
	@NotEmpty(message = "Email is required")
	private String email;

	@NotEmpty(message = "UserType is required")
	@Column(name = "USER_TYPE")
	private String userType;

	@Column(name = "REGISTRATION_DATE")
	private LocalDate registrationDate;

	@Column(name = "USER_STATUS")
	private String userStatus;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public LocalDate getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(LocalDate registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", password=" + password + ", mobileNo=" + mobileNo
				+ ", email=" + email + ", userType=" + userType + ", registrationDate=" + registrationDate
				+ ", userStatus=" + userStatus + "]";
	}

}
