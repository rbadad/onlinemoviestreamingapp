package com.online.moviestreaming.exception;

public class UserRegistrationFaliedException extends Exception {

	private static final long serialVersionUID = 1L;

	public UserRegistrationFaliedException() {
		super();
	}

	public UserRegistrationFaliedException(String msg) {
		super(msg);
	}

}
