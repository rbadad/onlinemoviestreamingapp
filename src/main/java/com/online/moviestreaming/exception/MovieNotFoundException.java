package com.online.moviestreaming.exception;

public class MovieNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String message;
	public MovieNotFoundException() {
		
	}
	
	public MovieNotFoundException(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "MovieNotFoundException [message=" + message + "]";
	}
}
