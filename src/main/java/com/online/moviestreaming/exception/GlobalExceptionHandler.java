package com.online.moviestreaming.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.online.moviestreaming.constants.AppConstants;

public class GlobalExceptionHandler {

	@ExceptionHandler(value = UserNotFoundException.class)
	public ResponseEntity<String> handleUserNotFoundException() {
		return new ResponseEntity<>(AppConstants.USER_NOT_FOUND,HttpStatus.OK);
	}
	@ExceptionHandler(value = MovieNotFoundException.class)
	public ResponseEntity<String> handleMovieNotFoundException() {
		return new ResponseEntity<>(AppConstants.MOVIE_NOT_FOUND,HttpStatus.OK);
	}
}
