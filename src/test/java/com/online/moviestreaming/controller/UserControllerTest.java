package com.online.moviestreaming.controller;

import static org.mockito.Mockito.doReturn;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.online.moviestreaming.dto.LoginRequestDto;
import com.online.moviestreaming.dto.ResponseDto;
import com.online.moviestreaming.service.UserService;
import com.online.moviestreaming.service.UserServiceImpl;

@SpringBootTest
class UserControllerTest {

	@InjectMocks
	private UserController userController;
	
	@Mock
	private UserServiceImpl userServiceImpl;
	
	@Mock
	private UserService userService;
	
	@BeforeEach
	public void dosetups() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void userLoginTest() {
		ResponseDto responseDto = new ResponseDto();
		LoginRequestDto requestDto = new LoginRequestDto();
		requestDto.setEmail("janbee@gmail.com");
		requestDto.setPassword("janbee123");
		doReturn(responseDto).when(userServiceImpl).login(requestDto);
		userController.login(requestDto);
	}
}
