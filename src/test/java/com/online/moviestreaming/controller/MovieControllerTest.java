package com.online.moviestreaming.controller;

import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.online.moviestreaming.entity.Movie;
import com.online.moviestreaming.service.MovieServiceImpl;

@SpringBootTest
class MovieControllerTest {

	@InjectMocks
	private MovieController movieController; 
	
	@Mock
	private MovieServiceImpl movieServiceImpl;
	
	@Before(value = "")
	public void dosetups() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	void testgetAllMovies() {
		Movie movie = new Movie();
		movie.setMovieId(1);
		List<Movie> movieList = new ArrayList<Movie>();
		movieList.add(movie);
		doReturn(movieList).when(movieServiceImpl).getAllMovies();
		movieController.getAllMovies();
	}
	
	@Test
	public void testgetMovieById() {
		Movie movie= new Movie();
		movie.setMovieId(1);
		try {
			doReturn(movie).when(movieServiceImpl).getMoviedBasedOnId(1);
			movieController.getMovieById(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testsearchMovies() {
		List<Movie> movieList = new ArrayList<>();
		Movie movie= new Movie();
		movie.setMovieId(1);
		movieList.add(movie);
		try {
			doReturn(movieList).when(movieServiceImpl).searchMovies("action");
		} catch (Exception e) {
			e.printStackTrace();
		}
		movieController.searchMovies("action");
	}

}
