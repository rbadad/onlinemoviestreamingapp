package com.online.moviestreaming.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.online.moviestreaming.dto.UserRegistrationRequestDto;
import com.online.moviestreaming.dto.UserRegistrationResponseDto;
import com.online.moviestreaming.entity.User;
import com.online.moviestreaming.exception.UserRegistrationFaliedException;
import com.online.moviestreaming.repository.RegistrationRepository;
import com.online.moviestreaming.service.RegistrationService;

public class RegistrationControllerTest {

	@InjectMocks
	RegistrationController registrationController;

	@Mock
	RegistrationService registrationService;

	@Mock
	RegistrationRepository registrationRepository;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testRegisterUserController() throws UserRegistrationFaliedException {
		UserRegistrationRequestDto requestDto = new UserRegistrationRequestDto();
		UserRegistrationResponseDto responseDto = new UserRegistrationResponseDto();
		responseDto.setMessage("User Registered Successfully");
		responseDto.setStatusCode("1000");
		User user = new User();

		user.setUserName("Prabir");
		user.setPassword("123456");
		user.setMobileNo("1234567890");
		requestDto.setUserName(user.getUserName());
		requestDto.setPassword(user.getPassword());
		requestDto.setMobileNo(user.getMobileNo());
		User userEntity = spy(new User());
		userEntity.setUserName(requestDto.getUserName());
		userEntity.setPassword(requestDto.getPassword());
		userEntity.setMobileNo(requestDto.getMobileNo());
		when(registrationRepository.save(userEntity)).thenReturn(user);
		when(registrationService.registerUser(requestDto)).thenReturn(responseDto);
		registrationController.registerUser(requestDto);
		verify(registrationService, times(1)).registerUser(requestDto);
		assertTrue(true);
	}
}
