package com.online.moviestreaming.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.online.moviestreaming.constants.AppConstants;
import com.online.moviestreaming.dto.LoginRequestDto;
import com.online.moviestreaming.dto.ResponseDto;
import com.online.moviestreaming.entity.User;
import com.online.moviestreaming.repository.UserRepository;

@SpringBootTest
class UserServiceImplTest {
	
	@InjectMocks
	UserServiceImpl userService;

	@Mock
	UserRepository userRepository;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testLoginValidationSuccess() {
		LoginRequestDto loginRequestDto = new LoginRequestDto();
		loginRequestDto.setEmail("Test@test.com");
		loginRequestDto.setPassword("123456");

		User user = new User();
		user.setEmail("Test@test.com");
		user.setPassword("123456");
		user.setUserId(1);
		//user.setMobileNo(565486856);
		user.setPassword("test");
		user.setRegistrationDate(LocalDate.now());
		user.setUserName("test");
		user.setUserStatus("active");
		user.setUserType("trial");
		when(userRepository.findByEmailAndPassword(loginRequestDto.getEmail(), loginRequestDto.getPassword())).thenReturn(Optional.of(user));
		
		ResponseDto responseDto = userService.login(loginRequestDto);
		assertEquals(AppConstants.LOGIN_SUCCESS, responseDto.getStatusMsg());
		assertEquals(AppConstants.LOGIN_SUCCESS_STATUS_CODE, responseDto.getStatusCode());
	}
	
	@Test
	public void testLoginValidationSuccess1() {
		LoginRequestDto loginRequestDto = new LoginRequestDto();
		loginRequestDto.setEmail("Test@test.com");
		loginRequestDto.setPassword("123456");

		User user = new User();
		user.setEmail("Test@test.com");
		user.setPassword("123456");
		user.setUserId(1);
		//user.setMobileNo(565486856);
		user.setPassword("test");
		user.setRegistrationDate(LocalDate.now());
		user.setUserName("test");
		user.setUserStatus("active");
		user.setUserType("paid");
		when(userRepository.findByEmailAndPassword(loginRequestDto.getEmail(), loginRequestDto.getPassword())).thenReturn(Optional.of(user));
		
		ResponseDto responseDto = userService.login(loginRequestDto);
		assertEquals(AppConstants.LOGIN_SUCCESS, responseDto.getStatusMsg());
		assertEquals(AppConstants.LOGIN_SUCCESS_STATUS_CODE, responseDto.getStatusCode());
	}
	
	@Test
	public void testLoginValidationSuccess2() {
		LoginRequestDto loginRequestDto = new LoginRequestDto();
		loginRequestDto.setEmail("Test@test.com");
		loginRequestDto.setPassword("123456");

		User user = new User();
		user.setEmail("Test@test.com");
		user.setPassword("123456");
		user.setUserId(1);
		//user.setMobileNo(565486856);
		user.setPassword("test");
		user.setRegistrationDate(LocalDate.of(2017, 1, 13));
		user.setUserName("test");
		user.setUserStatus("active");
		user.setUserType("paid");
		when(userRepository.findByEmailAndPassword(loginRequestDto.getEmail(), loginRequestDto.getPassword())).thenReturn(Optional.of(user));
		
		ResponseDto responseDto = userService.login(loginRequestDto);
		assertEquals(AppConstants.LOGIN_SUCCESS, responseDto.getStatusMsg());
		assertEquals(AppConstants.LOGIN_SUCCESS_STATUS_CODE, responseDto.getStatusCode());
	}
	
	@Test
	public void testLoginValidationSuccess3() {
		LoginRequestDto loginRequestDto = new LoginRequestDto();
		loginRequestDto.setEmail("Test@test.com");
		loginRequestDto.setPassword("123456");

		User user = new User();
		user.setEmail("Test@test.com");
		user.setPassword("123456");
		user.setUserId(1);
		//user.setMobileNo(565486856);
		user.setPassword("test");
		user.setRegistrationDate(LocalDate.of(2017, 1, 13));
		user.setUserName("test");
		user.setUserStatus("active");
		user.setUserType("trial");
		when(userRepository.findByEmailAndPassword(loginRequestDto.getEmail(), loginRequestDto.getPassword())).thenReturn(Optional.of(user));
		
		ResponseDto responseDto = userService.login(loginRequestDto);
		assertEquals(AppConstants.REGISTRATION_EXPIRED, responseDto.getStatusMsg());
		assertEquals(AppConstants.EXPIRED_STATUS_CODE, responseDto.getStatusCode());
	}
	
	
	@Test
	public void testLoginValidationFailure() {
		LoginRequestDto loginRequestDto = new LoginRequestDto();
		loginRequestDto.setEmail("Test@test.com");
		loginRequestDto.setPassword("123456");

		User user = new User();
		user.setEmail("Test1@test1.com");
		user.setPassword("123456");
		when(userRepository.findByEmailAndPassword(user.getEmail(), user.getPassword())).thenReturn(Optional.of(user));

		ResponseDto responseDto = userService.login(loginRequestDto);
		assertEquals(AppConstants.LOGIN_FAILURE, responseDto.getStatusMsg());
		assertEquals(AppConstants.LOGIN_FAILURE_STATUS_CODE, responseDto.getStatusCode());
	}

}
