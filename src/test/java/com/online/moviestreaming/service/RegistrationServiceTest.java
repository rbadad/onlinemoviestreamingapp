package com.online.moviestreaming.service;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.online.moviestreaming.dto.UserRegistrationRequestDto;
import com.online.moviestreaming.entity.User;
import com.online.moviestreaming.repository.RegistrationRepository;

public class RegistrationServiceTest {
	@InjectMocks
	RegistrationService registrationService;

	@Mock
	RegistrationRepository registrationRepository;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testRegisterUser() {
		UserRegistrationRequestDto requestDto = new UserRegistrationRequestDto();
		User user = new User();

		user.setUserName("Prabir");
		user.setPassword("123456");
		user.setMobileNo("1234567890");
		requestDto.setUserName(user.getUserName());
		requestDto.setPassword(user.getPassword());
		requestDto.setMobileNo(user.getMobileNo());
		when(registrationRepository.save(user)).thenReturn(user);
		assertTrue(true);

	}

}
